package com.chlaliotis.printecusecase

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [mainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var salesflowbtn: Button
    private lateinit var refundbtn: Button
    private lateinit var settingbtn: Button


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        salesflowbtn = view.findViewById(R.id.salesflowbutton)
        refundbtn = view.findViewById(R.id.refundflowbutton)
        settingbtn = view.findViewById(R.id.settingbutton)


        salesflowbtn.setOnClickListener{
             val action = MainFragmentDirections.actionMainFragmentTosalesflowFragment()
             findNavController().navigate(action)

         }
        refundbtn.setOnClickListener {
           val action  = MainFragmentDirections.actionMainFragmentTorefundflowFragment()
            findNavController().navigate(action)
        }
        settingbtn.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentTosettingsflowFragment()
            findNavController().navigate(action)


        }


    }
}