package com.chlaliotis.printecusecase

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.chlaliotis.printecusecase.utils.hideSoftKeyboard
import com.chlaliotis.printecusecase.viewmodels.PaymentViewModel
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SalesFlowFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class SalesFlowFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private val viewModel by viewModels<PaymentViewModel>()
    private lateinit var amountText: EditText
    private lateinit var payButton: Button
    private lateinit var displayView: TextView
    private lateinit var spinner: ProgressBar



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sales_flow, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        payButton = view.findViewById(R.id.refundValidateButton)
        displayView = view.findViewById(R.id.validateResult)
        amountText = view.findViewById(R.id.receiptText)

        spinner = view.findViewById(R.id.fundFlowProgressBar)
        payButton.setOnClickListener {
              activity?.hideSoftKeyboard()
               val amoutToPay = amountText.text.toString()
               viewModel.pay(amoutToPay)
           }
        viewModel?.payResult.observe(viewLifecycleOwner, Observer {
            val payResult = it ?: return@Observer
            val errordisplay = if (it.error == null) false else true
            when (errordisplay) {
                true -> {
                    with(displayView) {
                        visibility = View.VISIBLE
                        text =it.errordesciption
                        setTextColor(Color.RED)


                    }.also {
                        amountText.visibility = View.GONE
                        payButton.visibility = View.GONE

                    }

                }
                false -> {
                    with(displayView) {
                        visibility = View.VISIBLE
                        text =
                            "You have paid ${it.amount + it.currency} with receipt \n ${it.receiptNumber} "

                    }.also {
                        amountText.visibility = View.GONE
                        payButton.visibility = View.GONE

                    }

                }


            }


        })
        viewModel?.showLoading.observe(viewLifecycleOwner, Observer {
            when(it){
                true -> {
                    with(spinner){
                        visibility = View.VISIBLE
                    }

                }
                false ->{
                    with(spinner){
                        visibility = View.GONE
                    }
                }

            }


        })
    }


}