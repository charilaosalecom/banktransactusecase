package com.chlaliotis.printecusecase

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.chlaliotis.printecusecase.utils.hideSoftKeyboard
import com.chlaliotis.printecusecase.viewmodels.RefundViewModel
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RefundFlowFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class RefundFlowFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private val viewModel by viewModels<RefundViewModel>()
    private lateinit var receiptText: EditText
    private lateinit var validateButton: Button
    private lateinit var displayView: TextView
    private lateinit var spinner: ProgressBar
    private  lateinit var refundButton: Button
    private lateinit var amountToRefund: EditText
    private lateinit var  endMessage: TextView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_refund_flow, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinner = view.findViewById(R.id.fundFlowProgressBar)
        receiptText = view.findViewById(R.id.receiptText)
        refundButton = view.findViewById(R.id.refundTopayButton)
        amountToRefund = view.findViewById(R.id.amountRefundText)
        endMessage = view.findViewById(R.id.validateResult)

        validateButton = view.findViewById(R.id.refundValidateButton)
        validateButton.setOnClickListener {
            activity?.hideSoftKeyboard()

            val receiptToValidate = receiptText.text.toString()
            if (!receiptToValidate.isNullOrEmpty()) {
                viewModel.receiptValidate(receiptToValidate)
            }
        }
        refundButton.setOnClickListener {
            activity?.hideSoftKeyboard()

            val amountoRefund = amountToRefund.text.toString()
            if (!amountoRefund.isNullOrEmpty()) {
                viewModel.makerefund(amountoRefund)
            }
        }
        viewModel?.showLoading.observe(viewLifecycleOwner, Observer {
            when(it){
                true -> {
                    with(spinner){
                        visibility = View.VISIBLE
                    }

                }
                false ->{
                    with(spinner){
                        visibility = View.GONE
                    }
                }

            }


        })
        viewModel?.isReceiptValidated.observe(viewLifecycleOwner, Observer {
            when(it){
               true ->{
                   validateButton.visibility = View.GONE
                   receiptText.visibility = View.GONE
                   refundButton.visibility = View.VISIBLE
                   amountToRefund.visibility =View.VISIBLE
               }
                false ->{
                    validateButton.visibility = View.VISIBLE
                    receiptText.visibility = View.VISIBLE
                    receiptText.setTextColor(Color.RED)
                    refundButton.visibility = View.GONE
                    amountToRefund.visibility =View.GONE

                }


            }

        })
        viewModel?.isTransactionValidated.observe(viewLifecycleOwner, Observer {
            when(it){
                true ->{
                    validateButton.visibility = View.GONE
                    receiptText.visibility = View.GONE
                    receiptText.setTextColor(Color.RED)
                    refundButton.visibility = View.GONE
                    amountToRefund.visibility =View.GONE
                    with(endMessage){
                        visibility = View.VISIBLE
                        setText("Your refund transaction has been completed")
                        setTextColor(Color.GREEN)
                    }


                }
                false ->{
                   amountToRefund.setTextColor(Color.RED)
                }


            }

        })


    }


}