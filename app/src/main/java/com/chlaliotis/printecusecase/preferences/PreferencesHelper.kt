package com.chlaliotis.printecusecase.preferences

interface PreferencesHelper {

    fun saveMinAmount(amount: String)

    fun getMinAmount():String

    fun saveMaxAmount(amount: String)

    fun getMaxAmount():String

    fun saveUrl(url:String)

    fun getUrl():String
}