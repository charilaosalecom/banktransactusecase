package com.chlaliotis.printecusecase.preferences

import android.content.SharedPreferences
import javax.inject.Inject

const val MIN_AMOUNT = "minAmount"
const val MAX_AMOUNT = "maxAmount"
const val BASE_URL = "baseUrl"

class PreferencesHelperImpl @Inject constructor(
    private val preference: SharedPreferences
) : PreferencesHelper {

    override fun saveMinAmount(amount: String) {
        preference.edit()
            .putString(MIN_AMOUNT, amount)
            .apply()
    }

    override fun getMinAmount(): String {
        return preference.getString(MIN_AMOUNT, "") ?: ""
    }


    override fun saveMaxAmount(amount: String) {
        preference.edit()
            .putString(MAX_AMOUNT, amount)
            .apply()
    }

    override fun getMaxAmount(): String {
        return preference.getString(MAX_AMOUNT, "") ?: ""
    }

    override fun saveUrl(url: String) {
        preference.edit()
            .putString(BASE_URL, url)
            .apply()
    }

    override fun getUrl(): String {
        return preference.getString(BASE_URL, "https://api-vpigadas.herokuapp.com") ?: "https://api-vpigadas.herokuapp.com"

    }
}