package com.chlaliotis.printecusecase.data.model

data class PaymentFormState(val amount:String = "", val isPaymentValid: Boolean =false)