package com.chlaliotis.printecusecase.data.model

import com.squareup.moshi.JsonClass


open class Payment(val receiptNumber: String = "",val amount:String ="", val currency:String ="EUR",val error: Int? = null, val errordesciption:String? =null)
