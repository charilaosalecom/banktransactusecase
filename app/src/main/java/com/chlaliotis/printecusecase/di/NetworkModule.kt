package com.chlaliotis.printecusecase.di

import android.app.Application
import android.content.SharedPreferences
import com.chlaliotis.printecusecase.MainApplication
import com.chlaliotis.printecusecase.preferences.PreferencesHelper
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {

    const val URL = "https://api-vpigadas.herokuapp.com"

    @Provides
    @Singleton
    fun providesMainApplication(): MainApplication = MainApplication()


    @Provides
    @Singleton
    fun provideKotlinJsonAdapterFactory(): KotlinJsonAdapterFactory = KotlinJsonAdapterFactory()

    @Provides
    @Singleton
    fun provideMoshi(kotlinJsonAdapterFactory: KotlinJsonAdapterFactory): Moshi = Moshi.Builder()
            .add(kotlinJsonAdapterFactory)
            .build()


    @Provides
    @Singleton
    fun providesLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }


    @Provides
    @Singleton
    fun providesOkhttpInterceptor() : Interceptor{
        return  Interceptor { chain: Interceptor.Chain ->
            val original: Request = chain.request()
            val requestBuilder: Request.Builder = original.newBuilder()
                    .addHeader("Accept", "application/json")
            val request: Request = requestBuilder.build()
            chain.proceed(request)

        }
    }


    @Provides
    @Singleton
    fun provideMoshiConverterFactory(moshi: Moshi): MoshiConverterFactory =
            MoshiConverterFactory.create(moshi)

    @Provides
    @Singleton
    fun provideOkHttp(
            loggingInterceptor: HttpLoggingInterceptor,
            interceptor: Interceptor
    ): OkHttpClient =
            OkHttpClient
                    .Builder().addInterceptor(loggingInterceptor)
                    .addInterceptor(interceptor)
                    .build()

    @Provides
    @Singleton
    fun provideRetrofitClient(
            okHttp: OkHttpClient,
            moshiConverterFactory: MoshiConverterFactory, preferencesHelper: PreferencesHelper
    ): Retrofit = Retrofit.Builder()
            .addConverterFactory(moshiConverterFactory)
            .client(okHttp)
            .baseUrl(URL)
            .build()
}

