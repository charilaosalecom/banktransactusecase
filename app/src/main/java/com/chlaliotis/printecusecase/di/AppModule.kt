package com.chlaliotis.printecusecase.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext


const val KEY_PREFERENCES = "app preferences"


@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Provides
    fun providesSharedPreferebces(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE)

    }
}