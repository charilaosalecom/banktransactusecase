package com.chlaliotis.printecusecase.di

import com.chlaliotis.printecusecase.data.UserDataSource
import com.chlaliotis.printecusecase.repositories.PayRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule{

    @Provides
    @Singleton
    fun providesPayRepository(userDataSource: UserDataSource) = PayRepository(userDataSource)

}