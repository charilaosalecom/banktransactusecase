package com.chlaliotis.printecusecase.di

import com.chlaliotis.printecusecase.networking.ServiceBuilder
import com.chlaliotis.printecusecase.networking.UserService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object ServiceModule{

    @Provides
    @Singleton
    fun provideUserService(serviceBuilder: ServiceBuilder): UserService = serviceBuilder.buildService(UserService::class.java)
}