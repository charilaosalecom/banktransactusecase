package com.chlaliotis.printecusecase.di

import com.chlaliotis.printecusecase.data.UserDataSource
import com.chlaliotis.printecusecase.networking.UserService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton
import javax.sql.DataSource


@Module
@InstallIn(ApplicationComponent::class)
object DataSourceModule{

    @Provides
    @Singleton
    fun providesUserDataSource(userService: UserService) : UserDataSource = UserDataSource(userService)
}