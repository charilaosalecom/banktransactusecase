package com.chlaliotis.printecusecase.repositories

import android.util.Log
import com.chlaliotis.printecusecase.data.UserDataSource
import com.chlaliotis.printecusecase.data.model.Payment
import com.chlaliotis.printecusecase.data.Result
import javax.inject.Inject

class PayRepository @Inject constructor(val dataSource: UserDataSource) {

    var payment: Payment? = null
        private set

    var receiptNumber: String? =null
         private set

    init {

        payment = null
        receiptNumber = null
    }



    suspend fun pay(amount: String): Result<Payment> {
        // handle login
        val result = dataSource.pay(amount)

        if (result is Result.Success) {
            setPayment(result.data)
        }

        return result
    }
    suspend fun receipt(receipt_number: String): Result<String> {
        // handle login
        val result = dataSource.receipt(receipt_number)
        if (result is Result.Success) {
           receiptNumber = receipt_number
        }


        return result
    }
    suspend fun refund(amount:String): Result<String>? {
        // handle login
        val result = receiptNumber?.let { dataSource.refund(it,amount) }
        Log.i("RECEIPT", result.toString())


        return result
    }


    private fun setPayment(payment: Payment) {
        this.payment = payment

    }
    private fun receiptNumber(receipt_number:String){
        this.receiptNumber = receipt_number
    }

}