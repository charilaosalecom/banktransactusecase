package com.chlaliotis.printecusecase.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.chlaliotis.printecusecase.data.Result
import com.chlaliotis.printecusecase.data.model.Payment
import com.chlaliotis.printecusecase.repositories.PayRepository
import kotlinx.coroutines.launch

class RefundViewModel  @ViewModelInject constructor(private val payRepository: PayRepository,@Assisted private val savedStateHandle: SavedStateHandle) :ViewModel(){

    private val _isReceiptValidated = MutableLiveData<Boolean>()
    val isReceiptValidated: LiveData<Boolean> = _isReceiptValidated

    private val _showLoading = MutableLiveData<Boolean>()
    val showLoading: LiveData<Boolean> = _showLoading

    private val _isTransactionValidated = MutableLiveData<Boolean>()
    val isTransactionValidated: LiveData<Boolean> = _isTransactionValidated




    fun receiptValidate(receipt_number:String){
        _showLoading.postValue(true)

        viewModelScope.launch {

            val result = payRepository.receipt(receipt_number)
            _showLoading.postValue(false)


            if (result is Result.Success) {
               _isReceiptValidated.value = true

            } else {
                _isReceiptValidated.value = false
            }
        }


    }
    fun makerefund(amount:String){
        _showLoading.postValue(true)

        viewModelScope.launch {

            val result = payRepository.refund(amount)
            _showLoading.postValue(false)


            if (result is Result.Success) {
                _isTransactionValidated.value = true

            } else {
                _isTransactionValidated.value = false
            }
        }


    }


}
