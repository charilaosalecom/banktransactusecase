package com.chlaliotis.printecusecase.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.chlaliotis.printecusecase.data.Result
import com.chlaliotis.printecusecase.data.model.Payment
import com.chlaliotis.printecusecase.data.model.PaymentFormState
import com.chlaliotis.printecusecase.networking.PayResult
import com.chlaliotis.printecusecase.preferences.PreferencesHelper
import com.chlaliotis.printecusecase.repositories.PayRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.math.max

class PaymentViewModel  @ViewModelInject constructor(private val payRepository: PayRepository, private val preferencesHelper: PreferencesHelper,@Assisted private val savedStateHandle: SavedStateHandle) :
    ViewModel() {

    private val _payform = MutableLiveData<PaymentFormState>()
    val payFormState: LiveData<PaymentFormState> = _payform

    private val _payResult = MutableLiveData<Payment>()
    val payResult: LiveData<Payment> = _payResult

    private val _showLoading = MutableLiveData<Boolean>()
    val showLoading: LiveData<Boolean> = _showLoading

    private var _isEligibleforPayment : Boolean = false


    fun pay(amount: String) {
        _isEligibleforPayment = true


        val minAmount = preferencesHelper.getMinAmount().toIntOrNull()
        val maxAmount = preferencesHelper.getMaxAmount().toIntOrNull()

        val realAmount = amount.toString().toFloatOrNull()

        realAmount?.let { ramount ->
            minAmount?.let { minamount ->
                _isEligibleforPayment = ramount > minamount


            }
            maxAmount?.let { maxamount ->
                _isEligibleforPayment = ramount < maxamount
            }
        }
        when (_isEligibleforPayment) {
            true -> {
                _showLoading.postValue(true)
                makePayRequuest(amount)
            }
            false -> {
                _payResult.value = Payment(error = 100, errordesciption = "Anount not in Range of specified limits")


            }
        }


    }
    fun makePayRequuest(amount:String){
        viewModelScope.launch {






            val result = payRepository.pay(amount)
            _showLoading.postValue(false)
            if (result is Result.Success) {
                _payResult.value = result.data

            } else {
                _payResult.value = Payment(error = 101)
            }
        }


    }


    }



