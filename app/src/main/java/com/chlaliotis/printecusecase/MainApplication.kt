package com.chlaliotis.printecusecase

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application(){

   lateinit var baseUrl : String
}
